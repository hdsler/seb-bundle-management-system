﻿using System.Collections.Generic;
using System.Linq;
using BLL.Mappers;
using BLL.Services.Interfaces;
using DTOs;
using Microsoft.AspNetCore.Mvc;

namespace Bms.Controllers
{
    public class BundleController: Controller
    {
        private readonly IProductService _productService;
        private readonly IBundleService _bundleService;

        public BundleController(IProductService productService, IBundleService bundleService)
        {
            _productService = productService;
            _bundleService = bundleService;
        }

        public IActionResult UpdateBundle(BundleDto bundleDto, List<FieldDto> fields)
        {
            var validationMessages = _bundleService.ValidateBundleUpdate(bundleDto, fields);
            return Json(validationMessages);
        }

        [HttpPost]
        public IActionResult GetBundle([FromBody]List<FieldDto> fields)
        {
            var bundle = _bundleService.GetBundleOffer(fields);
            var dto = bundle.ToDto();
            return Json(dto);
        }

        [HttpGet]
        public IActionResult GetProducts()
        {
            var products = _productService.GetAll();

            var dtos = products.Select(p => p.ToDto());
            return Json(dtos);
        }

        public IActionResult GetAllProducts()
        {
            var products = _productService.GetAll();
            var dtos = products.Select(p => p.ToDto());
            return Json(dtos);
        }
    }
}
