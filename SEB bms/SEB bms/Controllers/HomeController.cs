﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using SEB_bms.Models;

namespace SEB_bms.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            ViewData["Message"] = "SEB bundle management system";
            return View();
        }
        
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
