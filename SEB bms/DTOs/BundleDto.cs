﻿using System.Collections.Generic;

namespace DTOs
{
    public class BundleDto
    {
        public int Id { get; set; }
        public int Value { get; set; }
        public List<ProductDto> Products { get; set; }
    }
}
