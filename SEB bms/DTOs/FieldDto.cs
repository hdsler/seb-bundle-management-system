﻿namespace DTOs
{
    public class FieldDto
    {
        public string PublicId { get; set; }
        public string Value { get; set; }
    }
}
