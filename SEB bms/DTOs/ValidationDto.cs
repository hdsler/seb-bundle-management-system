﻿using System.Collections.Generic;

namespace DTOs
{
    public class ValidationMessageDto
    {
        //public string FieldPublicId { get; set; }
        public string ProductName { get; set; }
        public string ValidationMessages { get; set; }
    }

    public class ValidationDto
    {
        public ValidationDto()
        {
            ValdationMessages = new List<ValidationMessageDto>();
        }

        public List<ValidationMessageDto> ValdationMessages { get; set; }
    }
}
