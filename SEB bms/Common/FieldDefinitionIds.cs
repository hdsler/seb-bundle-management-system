﻿using System;
using System.Collections.Generic;

namespace Common
{
    public class PredefinedField
    {
        public Guid Id { get; }
        public bool RequiresExistenceConversionAsNodeValue { get; }

        public PredefinedField(Guid id, bool requiresConversion)
        {
            Id = id;
            RequiresExistenceConversionAsNodeValue = requiresConversion;
        }

    }
    public static class FieldDefinitionIds
    {
        public static Guid AgeId = new Guid("00000001-0314-4532-b9f2-c22649195f51");
        public static Guid StudentId = new Guid("00000002-0314-4532-b9f2-c22649195f51");
        public static Guid IncomeId = new Guid("00000003-0314-4532-b9f2-c22649195f51");
        
        public static Guid CurrentAccountId = new Guid("00000004-0314-4532-b9f2-c22649195f51");
        public static Guid CurrentAccountPlusId = new Guid("00000005-0314-4532-b9f2-c22649195f51");
        public static Guid JuniorSaverAccountId = new Guid("00000006-0314-4532-b9f2-c22649195f51");
        public static Guid StudentAccountId = new Guid("00000007-0314-4532-b9f2-c22649195f51");
        public static Guid PensionerAccountId = new Guid("00000008-0314-4532-b9f2-c22649195f51");

        public static List<Guid> FieldsRequiringConversionAsNodeValueForExistanceCalculation = new List<Guid>
        {
            CurrentAccountId,
            CurrentAccountPlusId,
            JuniorSaverAccountId,
            StudentAccountId,
            PensionerAccountId
        };
    }
}
