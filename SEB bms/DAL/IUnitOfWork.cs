﻿using DAL.Repository.Interfaces;

namespace DAL
{
    public interface IUnitOfWork
    {
        INodeRepository Nodes { get; }
        IProductRepository Products { get; }
        IBundleRepository Bundles { get; }
    }
}
