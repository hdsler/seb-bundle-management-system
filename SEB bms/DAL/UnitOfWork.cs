﻿using DAL.Repository;
using DAL.Repository.Interfaces;

namespace DAL
{
    public class UnitOfWork: IUnitOfWork
    {
        private readonly BmsContext _context;

        private INodeRepository _nodeRepository;
        private IProductRepository _productRepository;
        private IBundleRepository _bundleRepository;

        public UnitOfWork(BmsContext context)
        {
            _context = context;
        }

        public INodeRepository Nodes => _nodeRepository ?? (_nodeRepository = new NodeRepository(_context));
        public IProductRepository Products => _productRepository ?? (_productRepository = new ProductRepository(_context));
        public IBundleRepository Bundles => _bundleRepository ?? (_bundleRepository = new BundleRepository(_context));
    }
}
