﻿using DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace DAL
{
    public class BmsContext: DbContext
    {
        public DbSet<Product> Products { get; set; }
        public DbSet<Bundle> Bundles { get; set; }
        public DbSet<BundleProduct> BundleProducts { get; set; }
        public DbSet<FieldDefinition> FieldDefinitions { get; set; }
        public DbSet<Node> Nodes { get; set; }
        public DbSet<Question> Questions { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Filename=SEBbms.db");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BundleProduct>()
                .HasKey(t => new { t.BundleId, t.ProductId });

            modelBuilder.Entity<Node>().HasOne(n => n.Left);
            modelBuilder.Entity<Node>().HasOne(n => n.Right);
            modelBuilder.Entity<Node>().HasOne(n => n.Parent);
        }
    }
}
