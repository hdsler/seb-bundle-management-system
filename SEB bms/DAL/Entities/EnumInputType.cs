﻿namespace DAL.Entities
{
    public enum EnumInputType
    {
        Undefined = 0,
        Boolean = 1,
        Number = 2,
    }
}
