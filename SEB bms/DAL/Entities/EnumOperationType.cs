﻿namespace DAL.Entities
{
    public enum EnumOperationType
    {
        Undefined,
        GreaterThan,
        LessThan,
        And,
        Or,
        Equal
    }
}
