﻿namespace DAL.Entities
{
    public enum EnumDataType
    {
        Undefined = 1,
        Number,
        Boolean
    }
}
