﻿using System.Collections.Generic;

namespace DAL.Entities
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Node ValidationRuleNode { get; set; }
        public FieldDefinition FieldDefinition { get; set; }
        public ICollection<BundleProduct> BundleProducts { get; set; }
    }
}
