﻿namespace DAL.Entities
{
    public enum EnumOutputType
    {
        Undefined = 0,
        Boolean = 1,
        NumberRange = 2
    }
}
