﻿namespace DAL.Entities
{
    public class BundleProduct
    {
        public int BundleId { get; set; }
        public Bundle Bundle { get; set; }

        public int ProductId { get; set; }
        public Product Product { get; set; }
    }
}
