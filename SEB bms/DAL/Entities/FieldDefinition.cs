﻿namespace DAL.Entities
{
    public class FieldDefinition
    {
        public int Id { get; set; }
        public string PublicId { get; set; }
        public string Name { get; set; }
    }
}
