﻿using System.Collections.Generic;

namespace DAL.Entities
{
    public class Bundle
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Value { get; set; }
        public Node ValidationRuleNode { get; set; }
        public ICollection<BundleProduct> BundleProducts { get; set; }
    }
}
