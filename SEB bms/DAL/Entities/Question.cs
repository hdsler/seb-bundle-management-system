﻿namespace DAL.Entities
{
    public class Question
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public EnumInputType InputType { get; set; }
        public EnumOutputType OutputType { get; set; }
    }
}
