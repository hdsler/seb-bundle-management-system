﻿namespace DAL.Entities
{
    public class Node
    {
        public int Id { get; set; }
        public EnumOperationType EnumOperationType { get; set; }
        public int? Value { get; set; }

        public int? PlaceholderForFieldDefinitionId { get; set; }
        public FieldDefinition PlaceholderForFieldDefinition { get; set; }

        public int? ParentId { get; set; }
        public virtual Node Parent { get; set; }

        public int? LeftId { get; set; }
        public virtual Node Left { get; set; }

        public int? RightId { get; set; }
        public virtual Node Right { get; set; }
    }
}
