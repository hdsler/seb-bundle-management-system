﻿using System;
using System.Collections.Generic;
using System.Linq;
using DAL.Entities;
using DAL.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repository
{
    public class ProductRepository: Repository<Product>, IProductRepository
    {
        public ProductRepository(DbContext dbContext) : base(dbContext)
        {
        }

        public IEnumerable<Product> GetAllWithRules()
        {
            return Entities
                .Include(e => e.ValidationRuleNode)
                .Include(e => e.FieldDefinition)
                .Where(e => e.ValidationRuleNode != null);
        }

        public Product GetWithRules(int id)
        {
            return Entities
                .Include(e => e.ValidationRuleNode)
                .Include(e => e.ValidationRuleNode)
                .FirstOrDefault(e => e.Id == id);
        }

        public Product GetByName(string name)
        {
            return Entities.FirstOrDefault(e =>
                string.Equals(e.Name, name, StringComparison.InvariantCultureIgnoreCase));
        }
    }
}
