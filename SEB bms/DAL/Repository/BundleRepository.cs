﻿using System.Collections.Generic;
using DAL.Entities;
using DAL.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repository
{
    public class BundleRepository: Repository<Bundle>, IBundleRepository
    {
        public BundleRepository(DbContext dbContext) : base(dbContext)
        {
            
        }

        public override IEnumerable<Bundle> GetAll()
        {
            return Entities
                .Include(e => e.ValidationRuleNode)
                .Include(e => e.BundleProducts)
                .ThenInclude(e => e.Product);
        }
    }
}
