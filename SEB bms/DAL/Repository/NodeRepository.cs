﻿using System.Collections.Generic;
using DAL.Entities;
using DAL.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repository
{
    public class NodeRepository: Repository<Node>, INodeRepository
    {
        public NodeRepository(DbContext context) : base(context)
        {
        }

        public IEnumerable<Node> GetAllWithExistingParent()
        {
            return Find(n => n.ParentId != null)
                .Include(n => n.PlaceholderForFieldDefinition);
        }
    }
}
