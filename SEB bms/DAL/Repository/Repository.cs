﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using DAL.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repository
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected BmsContext BmsContext;
        protected readonly DbContext DbContext;
        protected readonly DbSet<TEntity> Entities;

        public Repository(DbContext dbContext)
        {
            DbContext = dbContext;
            BmsContext = (BmsContext)dbContext;
            Entities = dbContext.Set<TEntity>();
        }

        public virtual IQueryable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            return Entities.Where(predicate);
        }

        public virtual TEntity Get(int id)
        {
            return Entities.Find(id);
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            return Entities;
        }
    }
}
