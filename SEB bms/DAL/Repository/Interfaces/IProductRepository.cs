﻿using System.Collections.Generic;
using DAL.Entities;

namespace DAL.Repository.Interfaces
{
    public interface IProductRepository: IRepository<Product>
    {
        IEnumerable<Product> GetAllWithRules();
        Product GetWithRules(int id);
        Product GetByName(string name);
    }
}
