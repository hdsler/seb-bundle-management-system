﻿using System.Collections.Generic;
using DAL.Entities;

namespace DAL.Repository.Interfaces
{
    public interface INodeRepository: IRepository<Node>
    {
        IEnumerable<Node> GetAllWithExistingParent();
    }
}
