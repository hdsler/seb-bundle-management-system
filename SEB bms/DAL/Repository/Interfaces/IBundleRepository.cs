﻿using DAL.Entities;

namespace DAL.Repository.Interfaces
{
    public interface IBundleRepository: IRepository<Bundle>
    {
    }
}
