﻿using Microsoft.Extensions.DependencyInjection;

namespace DAL
{
    public static class ServicesConfigureHelper
    {
        public static void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<IUnitOfWork, UnitOfWork>();
        }
    }
}
