﻿using Common;
using DAL.Entities;

namespace DAL.Initializer
{
    public abstract class RulesInitializerBase
    {
        public readonly FieldDefinition AgeFieldDefinition;
        public readonly FieldDefinition StudentFieldDefinition;
        public readonly FieldDefinition IncomeFieldDefinition;

        public readonly FieldDefinition CurrentAccountFieldDefinition;
        public readonly FieldDefinition CurrentAccountPlusFieldDefinition;
        public readonly FieldDefinition JuniorSaverAccountFieldDefinition;
        public readonly FieldDefinition StudentAccountFieldDefinition;
        public readonly FieldDefinition PensionerAccountFieldDefinition;

        public RulesInitializerBase()
        {
            AgeFieldDefinition = new FieldDefinition { Name = "Age", PublicId = FieldDefinitionIds.AgeId.ToString() };
            StudentFieldDefinition = new FieldDefinition { Name = "Student", PublicId = FieldDefinitionIds.StudentId.ToString() };
            IncomeFieldDefinition = new FieldDefinition { Name = "Income", PublicId = FieldDefinitionIds.IncomeId.ToString() };
            CurrentAccountFieldDefinition = new FieldDefinition { Name = "Current Account", PublicId = FieldDefinitionIds.CurrentAccountId.ToString() };
            CurrentAccountPlusFieldDefinition = new FieldDefinition { Name = "Current Account Plus", PublicId = FieldDefinitionIds.CurrentAccountPlusId.ToString() };
            JuniorSaverAccountFieldDefinition = new FieldDefinition { Name = "JuniorSaverAccount", PublicId = FieldDefinitionIds.JuniorSaverAccountId.ToString() };
            StudentAccountFieldDefinition = new FieldDefinition { Name = "Student Account", PublicId = FieldDefinitionIds.StudentAccountId.ToString() };
            PensionerAccountFieldDefinition = new FieldDefinition { Name = "Pensioner Account", PublicId = FieldDefinitionIds.PensionerAccountId.ToString() };
        }

        public Node InitCurrentAccountProductRules()
        {
            var node = CreateIncomeAndAgeNodeRule(EnumOperationType.GreaterThan, 0, EnumOperationType.And, EnumOperationType.GreaterThan, 17);
            return node;
        }

        public Node InitCurrentAccountPlusProductRules()
        {
            var node = CreateIncomeAndAgeNodeRule(EnumOperationType.GreaterThan, 40000, EnumOperationType.And, EnumOperationType.GreaterThan, 17);
            return node;
        }

        public Node InitJuniorSaverAccountProductRules()
        {
            var node = CreateNodeOperation(AgeFieldDefinition, -1, EnumOperationType.LessThan, null, 18);
            return node;
        }

        public Node InitStudentAccountProductRules()
        {
            var isStudentNode = CreateNodeOperation(StudentFieldDefinition, -1, EnumOperationType.Equal, null, 1); // In this rule 1 is true, 0 is false
            var rightNode = CreateNodeOperation(AgeFieldDefinition, -1, EnumOperationType.GreaterThan, null, 17); // In this rule 1 is true, 0 is false
            var rootNode = CreateOperationNode(isStudentNode, EnumOperationType.And, rightNode);
            return rootNode;
        }

        public Node InitDebitCardBundle()
        {
            // layer 4
            var curAccNode = CreateNodeOperation(CurrentAccountFieldDefinition, -1, EnumOperationType.Equal, null, 1);
            var currAccPlusNode = CreateNodeOperation(CurrentAccountPlusFieldDefinition, -1, EnumOperationType.Equal, null, 1);
            var studentAccNode = CreateNodeOperation(StudentAccountFieldDefinition, -1, EnumOperationType.Equal, null, 1);
            var pensionerAccNode = CreateNodeOperation(PensionerAccountFieldDefinition, -1, EnumOperationType.Equal, null, 1);

            // layer 3
            var thirdLayerNodeOne = CreateOperationNode(curAccNode, EnumOperationType.Or, currAccPlusNode);
            var thirdLayerNodeTwo = CreateOperationNode(currAccPlusNode, EnumOperationType.Or, studentAccNode);
            var thirdLayerNodeThree = CreateOperationNode(studentAccNode, EnumOperationType.Or, pensionerAccNode);
            
            // layer 2
            var secondLayerNodeOne = CreateOperationNode(thirdLayerNodeOne, EnumOperationType.Or, thirdLayerNodeTwo);
            var secondLayerNodeTwo = CreateOperationNode(thirdLayerNodeTwo, EnumOperationType.Or, thirdLayerNodeThree);

            // layer 1
            var rootNode = CreateOperationNode(secondLayerNodeOne, EnumOperationType.Or, secondLayerNodeTwo);

            return rootNode;
        }

        public Node InitCreditCardProductRules()
        {
            var node = CreateIncomeAndAgeNodeRule(EnumOperationType.GreaterThan, 12000, EnumOperationType.And, EnumOperationType.GreaterThan, 17);
            return node;
        }

        public Node InitGoldCreditCardProductRules()
        {
            var node = CreateIncomeAndAgeNodeRule(EnumOperationType.GreaterThan, 40000, EnumOperationType.And, EnumOperationType.GreaterThan, 17);
            return node;
        }

        public Node InitJuniorSaverBundleRules()
        {
            var node = CreateNodeOperation(AgeFieldDefinition, -1, EnumOperationType.LessThan, null, 18);
            return node;
        }

        public Node InitStudentBundleRules()
        {
            var ageNode = CreateNodeOperation(AgeFieldDefinition, -1, EnumOperationType.GreaterThan, null, 17);
            var studentNode = CreateNodeOperation(StudentFieldDefinition, -1, EnumOperationType.Equal, null, 1);
            var rootNode = CreateOperationNode(ageNode, EnumOperationType.And, studentNode);
            return rootNode;
        }

        public Node InitClassicBundleRules()
        {
            var ageNode = CreateNodeOperation(AgeFieldDefinition, -1, EnumOperationType.GreaterThan, null, 17);
            var incomeNode = CreateNodeOperation(IncomeFieldDefinition, -1, EnumOperationType.GreaterThan, null, 0);
            var rootNode = CreateOperationNode(ageNode, EnumOperationType.And, incomeNode);
            return rootNode;
        }

        public Node InitClassicPlusBundleRules()
        {
            var node = CreateIncomeAndAgeNodeRule(EnumOperationType.GreaterThan, 12000, EnumOperationType.And, EnumOperationType.GreaterThan, 17);
            return node;
        }

        public Node InitGoldBundleRules()
        {
            var node = CreateIncomeAndAgeNodeRule(EnumOperationType.GreaterThan, 40000, EnumOperationType.And, EnumOperationType.GreaterThan, 17);
            return node;
        }

        private Node CreateIncomeAndAgeNodeRule(
            EnumOperationType incomeOperationType, int incomeValue,
            EnumOperationType operationTypeOnRootNode,
            EnumOperationType ageOperationType, int ageValue)
        {
            var leftNode = CreateNodeOperation(IncomeFieldDefinition, -1, incomeOperationType, null, incomeValue);
            var rigthNode = CreateNodeOperation(AgeFieldDefinition, -1, ageOperationType, null, ageValue);
            var rootNode = CreateOperationNode(leftNode, operationTypeOnRootNode, rigthNode);
            return rootNode;
        }

        public abstract Node CreateOperationNode(Node leftNode, EnumOperationType operationType, Node rightNode);

        public abstract Node CreateNodeOperation(FieldDefinition leftDefinition, int leftValue,
            EnumOperationType operationType, FieldDefinition rightDefinition, int rightValue);
    }
}
