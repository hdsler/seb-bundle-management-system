﻿using DAL.Entities;

namespace DAL.Initializer
{
    public class BundleInitializer
    {
        private readonly RulesInitializerBase _ruleInitializer;

        public Bundle JuniorSaverBundle;
        public Bundle StudentBundle;
        public Bundle ClassicBundle;
        public Bundle ClassicPlusBundle;
        public Bundle GoldBundle;

        public BundleInitializer(RulesInitializerBase ruleInitializer)
        {
            _ruleInitializer = ruleInitializer;
            InitializeBundles();
        }

        private void InitializeBundles()
        {
            // Bundles
            JuniorSaverBundle = new Bundle
            {
                Id = GetNextPoductId(),
                Name = "Junior Saver",
                ValidationRuleNode = _ruleInitializer.InitJuniorSaverBundleRules(),
                Value = 0
            };
            StudentBundle = new Bundle
            {
                Id = GetNextPoductId(),
                Name = "Student",
                ValidationRuleNode = _ruleInitializer.InitStudentBundleRules(),
                Value = 0
            };
            ClassicBundle = new Bundle
            {
                Id = GetNextPoductId(),
                Name = "Classic",
                ValidationRuleNode = _ruleInitializer.InitClassicBundleRules(),
                Value = 1
            };
            ClassicPlusBundle = new Bundle
            {
                Id = GetNextPoductId(),
                Name = "Classic Plus",
                ValidationRuleNode = _ruleInitializer.InitClassicPlusBundleRules(),
                Value = 2
            };
            GoldBundle = new Bundle
            {
                Id = GetNextPoductId(),
                Name = "Gold",
                ValidationRuleNode = _ruleInitializer.InitGoldBundleRules(),
                Value = 3
            };
        }

        public virtual int GetNextPoductId()
        {
            return 0;
        }
    }
}
