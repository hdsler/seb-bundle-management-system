﻿using DAL.Entities;

namespace DAL.Initializer
{
    public abstract class BundleProductsInitializerBase
    {
        private readonly BundleInitializer _bundleInitializer;
        private readonly ProductsInitializer _productsInitializer;

        public BundleProductsInitializerBase(BundleInitializer bundleInitializer, ProductsInitializer productsInitializer)
        {
            _bundleInitializer = bundleInitializer;
            _productsInitializer = productsInitializer;
        }

        public void Initialize()
        {
            Map(_bundleInitializer.JuniorSaverBundle, _productsInitializer.JuniorSaverAccount);
            Map(_bundleInitializer.StudentBundle, _productsInitializer.StudentAccountProduct);
            Map(_bundleInitializer.StudentBundle, _productsInitializer.DebitCardProduct);
            Map(_bundleInitializer.StudentBundle, _productsInitializer.CreditCardProduct);
            Map(_bundleInitializer.ClassicBundle, _productsInitializer.CurrentAccountProduct);
            Map(_bundleInitializer.ClassicBundle, _productsInitializer.DebitCardProduct);
            Map(_bundleInitializer.ClassicPlusBundle, _productsInitializer.CurrentAccountProduct);
            Map(_bundleInitializer.ClassicPlusBundle, _productsInitializer.DebitCardProduct);
            Map(_bundleInitializer.ClassicPlusBundle, _productsInitializer.CreditCardProduct);
            Map(_bundleInitializer.GoldBundle, _productsInitializer.CurrentAccountPlusProduct);
            Map(_bundleInitializer.GoldBundle, _productsInitializer.DebitCardProduct);
            Map(_bundleInitializer.GoldBundle, _productsInitializer.GoldCreditCardProduct);
        }

        public abstract void Map(Bundle bundle, Product product);
    }
}
