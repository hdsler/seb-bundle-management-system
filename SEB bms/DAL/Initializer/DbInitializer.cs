﻿using DAL.Entities;
using Microsoft.EntityFrameworkCore.Internal;

namespace DAL.Initializer
{
    public static class DbInitializer
    {
        public static void Initialize(BmsContext context)
        {
            context.Database.EnsureCreated();

            // Look for any students.
            if (context.Products.Any())
            {
                return;   // DB has been seeded
            }

            // Rules
            var rulesInitializer = new RulesInitializer(context);
            var productsInitializer = new ProductsInitializer(rulesInitializer);

            context.Products.AddRange(
                productsInitializer.CurrentAccountProduct, 
                productsInitializer.CurrentAccountPlusProduct, 
                productsInitializer.JuniorSaverAccount, 
                productsInitializer.StudentAccountProduct,
                productsInitializer.DebitCardProduct, 
                productsInitializer.CreditCardProduct, 
                productsInitializer.GoldCreditCardProduct);

            var bundleInitializer = new BundleInitializer(rulesInitializer);

            context.Bundles.AddRange(bundleInitializer.JuniorSaverBundle, bundleInitializer.StudentBundle, 
                bundleInitializer.ClassicBundle, bundleInitializer.ClassicPlusBundle, bundleInitializer.GoldBundle);
            context.SaveChanges();

            // Bundle-Products mappings
            var bundleProductsInitializer = new BundleProductsInitializer(context, bundleInitializer, productsInitializer);
            bundleProductsInitializer.Initialize();

            // Question
            context.Questions.AddRange(
                new Question { Text = "Age", InputType = EnumInputType.Number, OutputType  = EnumOutputType.NumberRange},
                new Question { Text = "Student", InputType = EnumInputType.Boolean, OutputType = EnumOutputType.Boolean },
                new Question { Text = "Income", InputType = EnumInputType.Number, OutputType = EnumOutputType.NumberRange }
            );
            context.SaveChanges();
        }
    }
}
