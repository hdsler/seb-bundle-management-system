﻿using DAL.Entities;

namespace DAL.Initializer
{
    public class ProductsInitializer
    {
        private readonly RulesInitializerBase _ruleInitializer;

        // Products
        public Product CurrentAccountProduct;
        public Product CurrentAccountPlusProduct;
        public Product JuniorSaverAccount;
        public Product StudentAccountProduct;
        public Product DebitCardProduct;
        public Product CreditCardProduct;
        public Product GoldCreditCardProduct;

        public ProductsInitializer(RulesInitializerBase ruleInitializer)
        {
            _ruleInitializer = ruleInitializer;
            InitializeProducts();
        }

        private void InitializeProducts()
        {
            CurrentAccountProduct = new Product
            {
                Id = GetNextPoductId(),
                Name = "Current Account",
                ValidationRuleNode = _ruleInitializer.InitCurrentAccountProductRules(),
                FieldDefinition = _ruleInitializer.CurrentAccountFieldDefinition
            };
            CurrentAccountPlusProduct = new Product
            {
                Id = GetNextPoductId(),
                Name = "Current Account Plus",
                ValidationRuleNode = _ruleInitializer.InitCurrentAccountPlusProductRules(),
                FieldDefinition = _ruleInitializer.CurrentAccountPlusFieldDefinition
            };
            JuniorSaverAccount = new Product
            {
                Id = GetNextPoductId(),
                Name = "Junior Saver Account",
                ValidationRuleNode = _ruleInitializer.InitJuniorSaverAccountProductRules(),
                FieldDefinition = _ruleInitializer.JuniorSaverAccountFieldDefinition
            };
            StudentAccountProduct = new Product
            {
                Id = GetNextPoductId(),
                Name = "Student Account",
                ValidationRuleNode = _ruleInitializer.InitStudentAccountProductRules(),
                FieldDefinition = _ruleInitializer.StudentAccountFieldDefinition
            };
            DebitCardProduct = new Product
            {
                Id = GetNextPoductId(),
                Name = "Debit Card",
                ValidationRuleNode = _ruleInitializer.InitDebitCardBundle()
            };
            CreditCardProduct = new Product
            {
                Id = GetNextPoductId(),
                Name = "Credit Card",
                ValidationRuleNode = _ruleInitializer.InitCreditCardProductRules()
            };
            GoldCreditCardProduct = new Product
            {
                Id = GetNextPoductId(),
                Name = "Gold Credit Card",
                ValidationRuleNode = _ruleInitializer.InitGoldCreditCardProductRules()
            };
        }
        
        public virtual int GetNextPoductId()
        {
            return 0;
        }
    }
}
