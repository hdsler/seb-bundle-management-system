﻿using System.Collections.Generic;
using DAL.Entities;

namespace DAL.Initializer
{
    public class BundleProductsInitializer : BundleProductsInitializerBase
    {
        private readonly BmsContext _bmsContext;

        public BundleProductsInitializer(BmsContext context, BundleInitializer bundleInitialier, ProductsInitializer productsInitializer)
            : base(bundleInitialier, productsInitializer)
        {
            _bmsContext = context;
        }

        public override void Map(Bundle bundle, Product product)
        {
            _bmsContext.BundleProducts.Add(new BundleProduct { Bundle = bundle, Product = product });
        }
    }
}
