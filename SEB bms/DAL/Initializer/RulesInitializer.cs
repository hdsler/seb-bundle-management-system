﻿using DAL.Entities;

namespace DAL.Initializer
{
    public class RulesInitializer: RulesInitializerBase
    {
        private readonly BmsContext _context;

        public RulesInitializer(BmsContext context)
        {
            _context = context;
        }

        public override Node CreateOperationNode(Node leftNode, EnumOperationType operationType, Node rightNode)
        {
            var rootNode = new Node
            {
                EnumOperationType = operationType,
                Left = leftNode,
                LeftId = leftNode.Id,
                Right = rightNode,
                RightId = rightNode.Id
            };
            _context.Add(rootNode);
            _context.SaveChanges();

            leftNode.Parent = rootNode;
            leftNode.ParentId = rootNode.Id;

            rightNode.Parent = rootNode;
            rightNode.ParentId = rootNode.Id;

            return rootNode;
        }

        public override Node CreateNodeOperation(FieldDefinition leftDefinition, int leftValue,
            EnumOperationType operationType, FieldDefinition rightDefinition, int rightValue)
        {
            //var rootNodeId = id++;
            var rootNode = new Node
            {
                EnumOperationType = operationType
            };
            _context.Add(rootNode);
            _context.SaveChanges();

            var leftNode = new Node
            {
                Value = leftValue,
                ParentId = rootNode.Id,
                Parent = rootNode
            };
            var rightNode = new Node
            {
                Value = rightValue,
                ParentId = rootNode.Id,
                Parent = rootNode
            };
            rootNode.Left = leftNode;
            rootNode.Right = rightNode;

            if (leftDefinition != null)
            {
                //rootNode.Left.PlaceholderForFieldDefinitionId = leftDefinition.Id;
                rootNode.Left.PlaceholderForFieldDefinition = leftDefinition;
            }

            if (rightDefinition != null)
            {
                //rootNode.Right.PlaceholderForFieldDefinitionId = rightDefinition.Id;
                rootNode.Right.PlaceholderForFieldDefinition = rightDefinition;
            }

            _context.SaveChanges();
            return rootNode;
        }
    }
}
