﻿using DAL.Initializer;

namespace BLL.Tests
{
    public class TestBundleInitializer: BundleInitializer
    {
        private int _currentBundleId = 1;

        public TestBundleInitializer(RulesInitializerBase ruleInitializer) : base(ruleInitializer)
        {
        }

        public override int GetNextPoductId()
        {
            return _currentBundleId++;
        }
    }
}
