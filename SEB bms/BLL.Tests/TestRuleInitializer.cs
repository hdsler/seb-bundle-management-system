﻿using DAL.Entities;
using DAL.Initializer;

namespace BLL.Tests
{
    public class TestRuleInitializer: RulesInitializerBase
    {
        private int _currentNodeId = 1;

        public TestRuleInitializer()
        {

        }

        public override Node CreateOperationNode(Node leftNode, EnumOperationType operationType, Node rightNode)
        {
            return NodesTestHelper.CreateOperationNode(leftNode, operationType, rightNode, ref _currentNodeId);
        }

        public override Node CreateNodeOperation(FieldDefinition leftDefinition, int leftValue,
            EnumOperationType operationType, FieldDefinition rightDefinition, int rightValue)
        {
            return NodesTestHelper.CreateNodeOperation(leftDefinition, leftValue, operationType, rightDefinition,
                rightValue, ref _currentNodeId);
        }
    }
}
