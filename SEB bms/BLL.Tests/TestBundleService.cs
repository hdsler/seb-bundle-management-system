﻿using System.Collections.Generic;
using System.Linq;
using BLL.Mappers;
using BLL.Services;
using BLL.Services.Interfaces;
using Common;
using DAL;
using DAL.Entities;
using DAL.Initializer;
using DAL.Repository.Interfaces;
using DTOs;
using Moq;
using Xunit;

namespace BLL.Tests
{
    public class TestBundleService
    {
        private readonly IBundleService _bundleService;
        private List<Product> _allProducts;
        private List<Bundle> _allBundles;

        public TestBundleService()
        {
            var mockedUnitOfWork = GetMockedUnitOfWork();
            _bundleService = new BundleService(mockedUnitOfWork);
        }

        [Theory]
        [InlineData(17, false, 0, "Junior Saver")]
        [InlineData(18, true, 0, "Student")]
        [InlineData(18, true, 1, "Classic")]
        [InlineData(18, false, 1, "Classic")]
        [InlineData(18, true, 12001, "Classic Plus")]
        [InlineData(18, false, 12001, "Classic Plus")]
        [InlineData(18, true, 40001, "Gold")]
        [InlineData(18, false, 40001, "Gold")]
        public void TestRecommendedBundle(int age, bool isStudent, int income, string expectedBundleName)
        {
            var fields = CreateFields(age, isStudent, income);
            var bundle = _bundleService.GetBundleOffer(fields);
            Assert.Equal(expectedBundleName, bundle.Name);
        }

        [Theory]
        [InlineData(17, false, 0, "Junior Saver", "Current Account", false)]
        [InlineData(17, false, 0, "Junior Saver", "Credit Card", false)]
        [InlineData(17, false, 0, "Junior Saver", "Debit Card", false)]
        public void TestUpdatingBundleByAddingAdditionalProduct(int age, bool isStudent, int income, string initialBundleName, string productToAddName, bool updateAllowed)
        {
            var bundle = _allBundles.First(b => b.Name == initialBundleName);
            var bundleDto = bundle.ToDto();

            var productToAdd = _allProducts.First(p => p.Name == productToAddName);
            var productToAddDto = productToAdd.ToDto();

            bundleDto.Products.Add(productToAddDto);

            var fields = CreateFields(age, isStudent, income);
            var validationDto = _bundleService.ValidateBundleUpdate(bundleDto, fields);
            Assert.Equal(updateAllowed, !validationDto.ValdationMessages.Any());
        }

        [Theory]
        [InlineData(18, true, 0, "Student", "Debit Card", true)]
        [InlineData(18, true, 0, "Student", "Credit Card", true)]
        [InlineData(18, true, 1, "Classic", "Debit Card", true)]
        [InlineData(17, true, 1, "Classic", "Current Account", false)]
        public void TestUpdatingBundleByRemovingProduct(int age, bool isStudent, int income, string initialBundleName, string productToRemoveName, bool updateAllowed)
        {
            var bundle = _allBundles.First(b => b.Name == initialBundleName);
            var bundleDto = bundle.ToDto();

            var productToRemove = bundleDto.Products.First(p => p.Name == productToRemoveName);
            bundleDto.Products.Remove(productToRemove);

            var fields = CreateFields(age, isStudent, income);
            var validationDto = _bundleService.ValidateBundleUpdate(bundleDto, fields);
            Assert.Equal(updateAllowed, !validationDto.ValdationMessages.Any());
        }

        [Theory]
        [InlineData(true, true)]
        [InlineData(false, false)]
        public void TestAddingDebitCardWithoutRequiredForItProductsNotAllowed(bool withExistingAccount, bool updateAllowed)
        {
            var bundle = _allBundles.First(b => b.Name == "Junior Saver");
            var bundleDto = bundle.ToDto();

            if (!withExistingAccount)
            {
                var productToRemove = bundleDto.Products.First(p => p.Name == "Junior Saver Account");
                bundleDto.Products.Remove(productToRemove);
            }

            var productToAdd = _allProducts.First(p => p.Name == "Debit Card");
            bundleDto.Products.Add(productToAdd.ToDto());

            var fields = CreateFields(17, false, 0);
            var validationDto = _bundleService.ValidateBundleUpdate(bundleDto, fields);
            Assert.Equal(updateAllowed, !validationDto.ValdationMessages.Any());
        }

        private static List<FieldDto> CreateFields(int age, bool isStudent, int income)
        {
            return new List<FieldDto>
            {
                new FieldDto
                {
                    PublicId = FieldDefinitionIds.AgeId.ToString(),
                    Value = age.ToString()
                },
                new FieldDto
                {
                    PublicId = FieldDefinitionIds.StudentId.ToString(),
                    Value = isStudent ? "yes" : "no"
                },
                new FieldDto
                {
                    PublicId = FieldDefinitionIds.IncomeId.ToString(),
                    Value = income.ToString()
                }
            };
        }

        private  IUnitOfWork GetMockedUnitOfWork()
        {
            var unitOfWorkMock = new Mock<IUnitOfWork>();
            var bundleRepositoryMock = new Mock<IBundleRepository>();
            var productRepositoryMock = new Mock<IProductRepository>();
            var nodeRepositoryMock = new Mock<INodeRepository>();

            unitOfWorkMock.Setup(m => m.Bundles).Returns(bundleRepositoryMock.Object);
            unitOfWorkMock.Setup(m => m.Products).Returns(productRepositoryMock.Object);
            unitOfWorkMock.Setup(m => m.Nodes).Returns(nodeRepositoryMock.Object);

            var rulesInitializer = new TestRuleInitializer();
            var productsInitializer = new TestProductsInitializer(rulesInitializer);
            var bundleInitializer = new TestBundleInitializer(rulesInitializer);

            _allBundles = new List<Bundle>
            {
                bundleInitializer.ClassicBundle,
                bundleInitializer.ClassicPlusBundle,
                bundleInitializer.GoldBundle,
                bundleInitializer.JuniorSaverBundle,
                bundleInitializer.StudentBundle
            };
            bundleRepositoryMock.Setup(m => m.GetAll()).Returns(_allBundles);
            foreach (var bundle in _allBundles)
            {
                bundleRepositoryMock.Setup(m => m.Get(bundle.Id)).Returns(bundle);
            }

            _allProducts = new List<Product>
            {
                productsInitializer.CreditCardProduct,
                productsInitializer.CurrentAccountPlusProduct,
                productsInitializer.CurrentAccountProduct,
                productsInitializer.DebitCardProduct,
                productsInitializer.GoldCreditCardProduct,
                productsInitializer.JuniorSaverAccount,
                productsInitializer.StudentAccountProduct
            };
            productRepositoryMock.Setup(m => m.GetAllWithRules()).Returns(_allProducts);
            foreach (var product in _allProducts)
            {
                productRepositoryMock.Setup(m => m.GetWithRules(product.Id)).Returns(product);
            }

            var bundleProductsInitializer = new TestBundleProductsInitializer(bundleInitializer, productsInitializer);
            bundleProductsInitializer.Initialize();

            var topNodes = _allProducts.Select(p => p.ValidationRuleNode).ToList();
            topNodes.AddRange(_allBundles.Select(b => b.ValidationRuleNode));
            var allNodes = new List<Node>();
            topNodes.ForEach(n => GetFlatNodes(n, allNodes));
            var nodesWithParent = allNodes.Where(p => p.Parent != null || p.ParentId != null).ToList();
            nodeRepositoryMock.Setup(n => n.GetAllWithExistingParent()).Returns(nodesWithParent);

            return unitOfWorkMock.Object;
        }

        private static void GetFlatNodes(Node node, List<Node> allNodes)
        {
            allNodes.Add(node);
            if (node.Left != null)
            {
                GetFlatNodes(node.Left, allNodes);
            }

            if (node.Right != null)
            {
                GetFlatNodes(node.Right, allNodes);
            }
        }
    }
}
