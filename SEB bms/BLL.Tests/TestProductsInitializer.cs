﻿using DAL.Initializer;

namespace BLL.Tests
{
    public class TestProductsInitializer: ProductsInitializer
    {
        public TestProductsInitializer(RulesInitializerBase ruleInitializer) : base(ruleInitializer)
        {
        }

        private int _currentProductId = 1;
        public override int GetNextPoductId()
        {
            return _currentProductId++;
        }

    }
}
