﻿using DAL.Entities;

namespace BLL.Tests
{
    public class NodesTestHelper
    {
        public static Node CreateOperationNode(Node leftNode, EnumOperationType operationType, Node rightNode, ref int id)
        {
            var rootNodeId = id++;
            var rootNode = new Node
            {
                Id = rootNodeId,
                EnumOperationType = operationType,
                Left = leftNode,
                LeftId = leftNode.Id,
                Right = rightNode,
                RightId = rightNode.Id
            };

            leftNode.Parent = rootNode;
            leftNode.ParentId = rootNode.Id;

            rightNode.Parent = rootNode;
            rightNode.ParentId = rootNode.Id;

            return rootNode;
        }

        public static Node CreateNodeOperation(int leftValue, EnumOperationType operationType, int rightValue, ref int id)
        {
            var rootNodeId = id++;
            var rootNode = new Node
            {
                Id = rootNodeId,
                EnumOperationType = operationType
            };
            var leftNode = new Node
            {
                Id = id++,
                Value = leftValue,
                ParentId = rootNodeId,
                Parent = rootNode
            };
            var rightNode = new Node
            {
                Id = id++,
                Value = rightValue,
                ParentId = rootNodeId,
                Parent = rootNode
            };
            rootNode.Left = leftNode;
            rootNode.Right = rightNode;

            return rootNode;
        }

        public static Node CreateNodeOperation(FieldDefinition leftDefinition, int leftValue, 
            EnumOperationType operationType, FieldDefinition rightDefinition, int rightValue,
            ref int id)
        {
            var rootNode = CreateNodeOperation(leftValue, operationType, rightValue, ref id);

            if (leftDefinition != null)
            {
                rootNode.Left.PlaceholderForFieldDefinitionId = leftDefinition.Id;
                rootNode.Left.PlaceholderForFieldDefinition = leftDefinition;
            }

            if (rightDefinition != null)
            {
                rootNode.Right.PlaceholderForFieldDefinitionId = rightDefinition.Id;
                rootNode.Right.PlaceholderForFieldDefinition = rightDefinition;
            }

            return rootNode;
        }
    }
}
