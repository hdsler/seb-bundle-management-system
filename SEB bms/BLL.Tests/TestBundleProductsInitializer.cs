﻿using System.Collections.Generic;
using DAL.Entities;
using DAL.Initializer;

namespace BLL.Tests
{
    public class TestBundleProductsInitializer: BundleProductsInitializerBase
    {
        public TestBundleProductsInitializer(BundleInitializer bundleInitializer, ProductsInitializer productsInitializer) 
            : base(bundleInitializer, productsInitializer)
        {
        }

        public override void Map(Bundle bundle, Product product)
        {
            if (bundle.BundleProducts == null)
            {
                bundle.BundleProducts = new List<BundleProduct>();
            }
            bundle.BundleProducts.Add(new BundleProduct
            {
                Bundle = bundle,
                BundleId = bundle.Id,
                Product = product,
                ProductId = product.Id
            });
            product.BundleProducts = bundle.BundleProducts;
        }
    }
}
