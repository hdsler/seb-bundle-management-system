﻿using System;
using System.Collections.Generic;
using BLL.Helpers;
using DAL.Entities;
using Xunit;

namespace BLL.Tests
{
    public class TestBooleanLogic
    {
        [Theory]
        [InlineData(1, EnumOperationType.LessThan, 2, true)]
        [InlineData(2, EnumOperationType.LessThan, 1, false)]
        [InlineData(1, EnumOperationType.Equal, 1, true)]
        [InlineData(1, EnumOperationType.Equal, 2, false)]
        [InlineData(2, EnumOperationType.GreaterThan, 1, true)]
        [InlineData(1, EnumOperationType.GreaterThan, 2, false)]
        public void TestCompareOperations(int leftValue, EnumOperationType operationType, int rightValue, bool expectedResult)
        {
            var id = 1;
            var node = NodesTestHelper.CreateNodeOperation(leftValue, operationType, rightValue, ref id);

            var result = EvaluationHelper.Evaluate(node, new Dictionary<Guid, int>());
            Assert.Equal(expectedResult, result);
        }

        [Theory]
        [InlineData(1, EnumOperationType.LessThan, 2, true)]
        [InlineData(2, EnumOperationType.LessThan, 1, false)]
        [InlineData(1, EnumOperationType.Equal, 1, true)]
        [InlineData(1, EnumOperationType.Equal, 2, false)]
        [InlineData(2, EnumOperationType.GreaterThan, 1, true)]
        [InlineData(1, EnumOperationType.GreaterThan, 2, false)]
        public void TestCompareOperationsWithValuesSubstitution(int leftValue, EnumOperationType operationType, int rightValue, bool expectedResult)
        {
            var fieldDefinitionOne = new FieldDefinition
            {
                Id = 1,
                Name = "DefOne",
                PublicId = Guid.NewGuid().ToString()
            };

            var fieldDefinitionTwo = new FieldDefinition
            {
                Id = 1,
                Name = "DefTwo",
                PublicId = Guid.NewGuid().ToString()
            };

            var id = 1;
            var node = NodesTestHelper.CreateNodeOperation(fieldDefinitionOne, -100, operationType, fieldDefinitionTwo, -200, ref id);
            
            var result = EvaluationHelper.Evaluate(node, new Dictionary<Guid, int>
            {
                { Guid.Parse(fieldDefinitionOne.PublicId), leftValue },
                { Guid.Parse(fieldDefinitionTwo.PublicId), rightValue },
            });
            Assert.Equal(expectedResult, result);
        }

        // a > b && c > d
        [Theory]
        [InlineData(1, 0, 18, 17, true)]
        [InlineData(0, 0, 18, 17, false)]
        [InlineData(1, 0, 16, 17, false)]
        [InlineData(1, 0, 5, 17, false)]
        public void TestAnd(int a, int b, int c, int d, bool expectedResult)
        {
            var currentId = 1;
            var leftNode = NodesTestHelper.CreateNodeOperation(a, EnumOperationType.GreaterThan, b, ref currentId);
            var rightNode = NodesTestHelper.CreateNodeOperation(c, EnumOperationType.GreaterThan, d, ref currentId);
            var node = NodesTestHelper.CreateOperationNode(leftNode, EnumOperationType.And, rightNode, ref currentId);

            var result = EvaluationHelper.Evaluate(node, new Dictionary<Guid, int>());
            Assert.Equal(expectedResult, result);
        }

        // a == b || c > d
        [Theory]
        [InlineData(1, 1, 9, 10, true)]
        [InlineData(2, 1, 11, 10, true)]
        [InlineData(2, 1, 9, 10, false)]
        public void TestOr(int a, int b, int c, int d, bool expectedResult)
        {
            var currentId = 1;
            var leftNode = NodesTestHelper.CreateNodeOperation(a, EnumOperationType.Equal, b, ref currentId);
            var rightNode = NodesTestHelper.CreateNodeOperation(c, EnumOperationType.GreaterThan, d, ref currentId);
            var node = NodesTestHelper.CreateOperationNode(leftNode, EnumOperationType.Or, rightNode, ref currentId);

            var result = EvaluationHelper.Evaluate(node, new Dictionary<Guid, int>());
            Assert.Equal(expectedResult, result);
        }
    }
}
