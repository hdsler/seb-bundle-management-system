﻿using System;
using System.Collections.Generic;
using DAL.Entities;

namespace BLL.Helpers
{
    public static class EvaluationHelper
    {
        public static bool Evaluate(Node rootNode, Dictionary<Guid, int> valuesByFieldId)
        {
            if (rootNode.Parent != null)
            {
                throw new InvalidOperationException("Root node must have a parent");
            }

            if (rootNode.Left == null || rootNode.Right == null)
            {
                throw new InvalidOperationException("Root node must have left and right nodes");
            }

            return CalculateRecursively(rootNode, valuesByFieldId);
        }

        private static bool CalculateRecursively(Node node, Dictionary<Guid, int> valuesByFieldId)
        {
            if (node == null || node.Left == null && node.Right == null)
            {
                return true;
            }

            if (node.Left != null && node.Right == null || node.Left == null && node.Right != null)
            {
                throw new InvalidOperationException("Left and right nodes either both must be null or both have a value");
            }

            //SetNodeValueIfRequired(node, valuesByFieldId);

            if (!HasMoreThanOneNodeDepth(node))
            {
                SetNodeValueIfRequired(node.Left, valuesByFieldId);
                SetNodeValueIfRequired(node.Right, valuesByFieldId);
                return Calculate(node.Left.Value.Value, node.Right.Value.Value, node.EnumOperationType);
            }

            var leftNodeResult = CalculateRecursively(node.Left, valuesByFieldId);
            var rightNodeResult = CalculateRecursively(node.Right, valuesByFieldId);

            switch (node.EnumOperationType)
            {
                case EnumOperationType.And:
                    return leftNodeResult && rightNodeResult;
                case EnumOperationType.Or:
                    return leftNodeResult || rightNodeResult;
                case EnumOperationType.Equal:
                    return leftNodeResult == rightNodeResult;
                default:
                    throw new InvalidOperationException("At this point only boolean operations are allowed");
            }
        }

        private static void SetNodeValueIfRequired(Node node, IReadOnlyDictionary<Guid, int> valuesByFieldId)
        {
            if (node.PlaceholderForFieldDefinitionId.HasValue && node.PlaceholderForFieldDefinition != null)
            {
                var definitionId = Guid.Parse(node.PlaceholderForFieldDefinition.PublicId);
                if (!valuesByFieldId.TryGetValue(definitionId, out var value))
                {
                    throw new Exception("Value by field definition id was not found in the dictionary");
                }
                node.Value = value;
            }
        }

        private static bool HasMoreThanOneNodeDepth(Node node)
        {
            return node?.Left?.Left != null && node.Right?.Right != null;
        }

        private static bool Calculate(int leftOperand, int rightOperand, EnumOperationType operatorType)
        {
            switch (operatorType)
            {
                case EnumOperationType.GreaterThan:
                    return leftOperand > rightOperand;
                case EnumOperationType.LessThan:
                    return leftOperand < rightOperand;
                case EnumOperationType.Equal:
                    return leftOperand == rightOperand;
                    
                default:
                    throw new ArgumentOutOfRangeException(nameof(operatorType), operatorType, null);
            }
        }
    }
}
