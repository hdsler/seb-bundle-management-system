﻿using System;

namespace BLL.Helpers
{
    public interface IInputConvertor
    {
        int Convert(string value);
    }

    public class IntConvertor: IInputConvertor
    {
        public int Convert(string value)
        {
            if (!int.TryParse(value, out var intValue))
            {
                throw new InvalidOperationException($"Can't convert {value} to integer");
            }

            return intValue;
        }
    }

    public class YesNoConvertor : IInputConvertor
    {
        public int Convert(string value)
        {
            if (string.Equals(value, "yes", StringComparison.CurrentCultureIgnoreCase))
            {
                return 1;
            }
            if (string.Equals(value, "no", StringComparison.CurrentCultureIgnoreCase))
            {
                return 0;
            }
            throw new InvalidOperationException($"Cant convert {value} string to boolean 1 or 0");
        }
    }
}
