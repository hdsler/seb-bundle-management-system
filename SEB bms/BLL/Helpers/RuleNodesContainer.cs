﻿using System.Linq;
using DAL;
using DAL.Entities;

namespace BLL.Helpers
{
    public class RuleNodesContainer
    {
        private readonly IUnitOfWork _unitOfWork;

        private ILookup<int?, Node> _ruleNodes;
        public ILookup<int?, Node> RuleNodes => _ruleNodes ?? (_ruleNodes = _unitOfWork.Nodes.GetAllWithExistingParent().ToLookup(n => n.ParentId));

        public RuleNodesContainer(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
    }
}
