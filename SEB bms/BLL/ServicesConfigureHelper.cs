﻿using BLL.Services;
using BLL.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace BLL
{
    public static class ServicesConfigureHelper
    {
        public static void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<INodeService, NodeService>();
            services.AddTransient<IProductService, ProductService>();
            services.AddTransient<IBundleService, BundleService>();
        }
    }
}
