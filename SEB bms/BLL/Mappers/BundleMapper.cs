﻿using System.Linq;
using DAL.Entities;
using DTOs;

namespace BLL.Mappers
{
    public static class BundleMapper
    {
        public static BundleDto ToDto(this Bundle bundle)
        {
            return new BundleDto
            {
                Id = bundle.Id,
                Value = bundle.Value,
                Products = bundle.BundleProducts.Select(p => p.Product.ToDto()).ToList()
            };
        }
    }
}
