﻿using DAL.Entities;
using DTOs;

namespace BLL.Mappers
{
    public static class ProductMapper
    {
        public static ProductDto ToDto(this Product product)
        {
            var dto = new ProductDto
            {
                Id = product.Id,
                Name = product.Name
            };
            return dto;
        }
    }
}
