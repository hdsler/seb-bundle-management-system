﻿using System;
using System.Collections.Generic;
using System.Linq;
using BLL.Helpers;
using DTOs;

namespace BLL.Mappers
{
    public static class FieldDtoMapper
    {
        public static Dictionary<Guid, int> ToValuesDictionary(this List<FieldDto> fieldDtos)
        {
            var valuesByFieldId = new Dictionary<Guid, int>();
            foreach (var fieldDto in fieldDtos)
            {
                var convertedValue = PredefinedFieldConvertors.ConvertorsByFieldId[fieldDto.PublicId].Single().Convert(fieldDto.Value);
                valuesByFieldId.Add(Guid.Parse(fieldDto.PublicId), convertedValue);
            }

            return valuesByFieldId;
        }
    }
}
