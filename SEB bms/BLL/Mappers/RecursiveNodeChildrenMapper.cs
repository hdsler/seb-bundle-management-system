﻿using System.Linq;
using DAL.Entities;

namespace BLL.Mappers
{
    public class RecursiveNodeChildrenMapper
    {
        public static void MapNodes(Node node, ILookup<int?, Node> ruleNodes)
        {
            if (node.LeftId == null && node.RightId == null)
            {
                return;
            }

            var leftNode = ruleNodes[node.Id].Single(n => n.Id == node.LeftId);
            var rightNode = ruleNodes[node.Id].Single(n => n.Id == node.RightId);

            node.Left = leftNode;
            node.Right = rightNode;

            MapNodes(leftNode, ruleNodes);
            MapNodes(rightNode, ruleNodes);
        }
    }
}
