﻿using System.Collections.Generic;
using System.Linq;
using BLL.Helpers;
using Common;

namespace BLL
{
    // TODO: storing of this mapping can be moved to database
    public class PredefinedFieldConvertors
    {
        private static readonly IInputConvertor IntegerConvertor = new IntConvertor();
        private static readonly IInputConvertor YesNoConvertor = new YesNoConvertor();

        public static readonly ILookup<string, IInputConvertor> ConvertorsByFieldId = new Dictionary<string, IInputConvertor>
        {
            {FieldDefinitionIds.AgeId.ToString(), IntegerConvertor},
            {FieldDefinitionIds.StudentId.ToString(),  YesNoConvertor },
            {FieldDefinitionIds.IncomeId.ToString(), IntegerConvertor },

            {FieldDefinitionIds.CurrentAccountId.ToString(), YesNoConvertor },
            {FieldDefinitionIds.CurrentAccountPlusId.ToString(), YesNoConvertor },
            {FieldDefinitionIds.JuniorSaverAccountId.ToString(), YesNoConvertor },
            {FieldDefinitionIds.StudentAccountId.ToString(), YesNoConvertor },
            {FieldDefinitionIds.PensionerAccountId.ToString(), YesNoConvertor }

        }.ToLookup(k => k.Key, v => v.Value) ;
    }
}
