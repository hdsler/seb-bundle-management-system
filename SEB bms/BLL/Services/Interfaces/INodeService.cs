﻿using System.Collections.Generic;
using DAL.Entities;

namespace BLL.Services.Interfaces
{
    public interface INodeService
    {
        List<Node> GetAll();
    }
}
