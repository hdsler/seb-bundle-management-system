﻿using System.Collections.Generic;
using DAL.Entities;
using DTOs;

namespace BLL.Services.Interfaces
{
    public interface IProductService
    {
        Product Get(int id);
        Product Get(string name);
        IEnumerable<Product> GetAll();
    }
}
