﻿using System.Collections.Generic;
using DAL.Entities;
using DTOs;

namespace BLL.Services.Interfaces
{
    public interface IBundleService
    {
        Bundle GetBundleOffer(List<FieldDto> fields);
        ValidationDto ValidateBundleUpdate(BundleDto bundle, List<FieldDto> fields);
    }
}
