﻿using System.Collections.Generic;
using BLL.Services.Interfaces;
using DAL;
using DAL.Entities;

namespace BLL.Services
{
    public class ProductService: IProductService
    {
        private readonly IUnitOfWork _unitOfWork;

        public ProductService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Product Get(int id)
        {
            return _unitOfWork.Products.Get(id);
        }

        public Product Get(string name)
        {
            return _unitOfWork.Products.GetByName(name);
        }

        public IEnumerable<Product> GetAll()
        {
            var products = _unitOfWork.Products.GetAll();
            return products;
        }
    }
}
