﻿using System.Collections.Generic;
using System.Linq;
using BLL.Services.Interfaces;
using DAL;
using DAL.Entities;

namespace BLL.Services
{
    public class NodeService: INodeService
    {
        private readonly IUnitOfWork _unitOfWork;

        public NodeService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public List<Node> GetAll()
        {
            var nodes = _unitOfWork.Nodes.GetAll().ToList();
            
            return nodes;
        }
    }
}
