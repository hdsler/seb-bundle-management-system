﻿using System;
using System.Collections.Generic;
using System.Linq;
using BLL.Helpers;
using BLL.Mappers;
using BLL.Services.Interfaces;
using Common;
using DAL;
using DAL.Entities;
using DTOs;

namespace BLL.Services
{
    public class BundleService: IBundleService
    {
        private readonly IUnitOfWork _unitOfWork;

        private readonly RuleNodesContainer _ruleNodesContainer;

        public BundleService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _ruleNodesContainer = new RuleNodesContainer(unitOfWork);
        }

        public Bundle GetBundleOffer(List<FieldDto> fields)
        {
            var bundles = _unitOfWork.Bundles.GetAll().ToList();
            foreach (var bundle in bundles)
            {
                if (bundle.ValidationRuleNode != null)
                {
                    RecursiveNodeChildrenMapper.MapNodes(bundle.ValidationRuleNode, _ruleNodesContainer.RuleNodes);
                }
            }

            var fieldValues = fields.ToValuesDictionary();
            var validatedBundles = bundles.Where(p => EvaluationHelper.Evaluate(p.ValidationRuleNode, fieldValues)).ToList();

            if (validatedBundles.Count > 1)
            {
                return validatedBundles.OrderByDescending(b => b.Value).First();
            }
            return validatedBundles.SingleOrDefault();
        }

        public ValidationDto ValidateBundleUpdate(BundleDto bundleDto, List<FieldDto> fields)
        {
            var validationDto = new ValidationDto();
            var initialBundle = _unitOfWork.Bundles.Get(bundleDto.Id);
            
            if (initialBundle.ValidationRuleNode != null)
            {
                RecursiveNodeChildrenMapper.MapNodes(initialBundle.ValidationRuleNode, _ruleNodesContainer.RuleNodes);
            }

            var allProducts = _unitOfWork.Products.GetAllWithRules().ToList();
            AddExistingProductsToFieldsValues(bundleDto, fields, allProducts);
            var fieldsDictionary = fields.ToValuesDictionary();

            var moduleValidated = EvaluationHelper.Evaluate(initialBundle.ValidationRuleNode, fieldsDictionary);
            if (!moduleValidated)
            {
                validationDto.ValdationMessages.Add(new ValidationMessageDto
                {
                    ValidationMessages = "You are not eligible for this bundle"
                });
                return validationDto;
            }

            var initialBundleProducts = initialBundle.BundleProducts.Select(p => p.Product).ToList();
            var addedProductIds = bundleDto.Products.Select(b => b.Id)
                .Except(initialBundleProducts.Select(b => b.Id)).ToList();

            foreach (var addedProductd in addedProductIds)
            { 
                var product = _unitOfWork.Products.GetWithRules(addedProductd);
                if (product.ValidationRuleNode != null)
                {
                    RecursiveNodeChildrenMapper.MapNodes(product.ValidationRuleNode, _ruleNodesContainer.RuleNodes);
                }
                var validated = EvaluationHelper.Evaluate(product.ValidationRuleNode, fieldsDictionary);
                if (!validated)
                {
                    validationDto.ValdationMessages.Add(new ValidationMessageDto
                    {
                        ProductName = product.Name,
                        ValidationMessages = "Not allowed"
                    });
                }
            }

            return validationDto;
        }

        public void AddExistingProductsToFieldsValues(BundleDto bundleDtos, List<FieldDto> fields, List<Product> allProducts)
        {
            var bundleProductIds = bundleDtos.Products.Select(p => p.Id).ToList();
            foreach (var product in allProducts)
            {
                if (product.FieldDefinition != null 
                    && FieldDefinitionIds.FieldsRequiringConversionAsNodeValueForExistanceCalculation.Contains(Guid.Parse(product.FieldDefinition.PublicId)))
                {
                    var value = bundleProductIds.Contains(product.Id) ? "yes" : "no";
                    fields.Add(new FieldDto { PublicId = product.FieldDefinition.PublicId, Value = value });
                }
            }

            // TODO: move it to db initializer
            fields.Add(new FieldDto { PublicId = FieldDefinitionIds.PensionerAccountId.ToString(), Value = "no" });
        }
    }
}
