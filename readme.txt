How to run an application
1. Open "Seb bms.sln" solution file with "Visual Studio"/Rider/etc
2. Right click on "Seb bms" project => Debug => Run

How to run tests
1. Open "Test Explorer"
2. Right click on BLL.Tests => "Run Selected Tests"

***Notes: 
1. Frontend is not implemented so it is not workable (no update/validations)
2. Some tests are failing
3. Validation messages are not implemented